package com.example.demo.person.service;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/person")
public class PersonService {
    @GetMapping
    public List<String> getStudentName(){
        return List.of("Sinai NIN","Naiseang PO","Eliana Nang");
    }
}
